﻿export module NODECPPCJSONDTS {
    interface cJSON {}
    interface charPtr {}
    
    function cJSON_Version() : string ;
    function cJSON_Parse( jsonStr: string ) : cJSON ;
    function cJSON_Print( p: cJSON ) : charPtr ;
    function cJSON_Minify( str: charPtr ) : void ;
    function cJSON_Delete( p: cJSON ) : void ;
    function cJSON_GetArraySize( p: cJSON ) : number ;
    function cJSON_GetArrayItem( p: cJSON, index : number ) : cJSON ;
    function cJSON_GetObjectItem(p: cJSON, key: string ) : cJSON;
    function cJSON_HasObjectItem(p: cJSON, key: string ) : number;
    function cJSON_GetErrorPtr() : string;

    function cJSON_GetStringValue(p: cJSON ) : string;
    function cJSON_GetNumberValue(p: cJSON ) : number;

    function cJSON_CreateObject() : cJSON ;
    function cJSON_AddStringToObject(p: cJSON, key: string, val:string) ;
    function cJSON_AddNumberToObject(p: cJSON, key: string, val:number) ;
    function cJSON_AddFalseToObject(p: cJSON, key: string) ;
    function cJSON_AddTrueToObject(p: cJSON, key: string) ;    
    
    function cJSON_IsInvalid(p: cJSON ) : number;
    function cJSON_IsBool(p: cJSON ) : number;
    function cJSON_IsFalse(p: cJSON ) : number;
    function cJSON_IsTrue(p: cJSON ) : number;
    function cJSON_IsNull(p: cJSON ) : number;
    function cJSON_IsNumber(p: cJSON ) : number;
    function cJSON_IsString(p: cJSON ) : number;
    function cJSON_IsArray(p: cJSON ) : number;
    function cJSON_IsObject(p: cJSON ) : number;
    function cJSON_IsRaw(p: cJSON ) : number;
    
    function cJSON_CreateNull() : cJSON;
    function cJSON_CreateTrue() : cJSON;
    function cJSON_CreateFalse() : cJSON;
    function cJSON_CreateBool(bl: number) : cJSON;
    function cJSON_CreateNumber(n: number) : cJSON;
    function cJSON_CreateString( str: string) : cJSON;

    function cJSON_CreateRaw( raw: string ) : cJSON;
    function cJSON_CreateArray( ) : cJSON;
    function cJSON_CreateObject( ) : cJSON;

    function cJSON_AddItemToArray(pArr: cJSON, item: cJSON ): number;
    function cJSON_AddItemToObject(pObj: cJSON, key: string, item: cJSON ): number;

    function cJSON_AddNullToObject(object: cJSON, name: string ): cJSON;
    function cJSON_AddTrueToObject(object: cJSON, name: string ): cJSON;
    function cJSON_AddFalseToObject(object: cJSON, name: string ): cJSON;
    function cJSON_AddNumberToObject(object: cJSON, name: string, num : number ): cJSON;
    function cJSON_AddStringToObject(object: cJSON, name: string, str : string ): cJSON;
    function cJSON_AddRawToObject(object: cJSON, name: string, raw : string ): cJSON;
    function cJSON_AddObjectToObject(object: cJSON, name: string ): cJSON;
    function cJSON_AddArrayToObject(object: cJSON, name: string ): cJSON;
}
