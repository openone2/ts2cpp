import { NODECPPCJSONDTS } from "./../third_party/cJSON/cJSON";
import { node_cpp_toolbox_define } from "./node_cpp_toolbox_define";

export module NodeCppJSON {

      export function jsonVerion() : string {
        return NODECPPCJSONDTS.cJSON_Version();
      }

      export function parse( jsonStr: string ) : NODECPPCJSONDTS.cJSON {
        return NODECPPCJSONDTS.cJSON_Parse( jsonStr );
      }

      export function stringify( p: NODECPPCJSONDTS.cJSON ) : string  {
        let cStr = NODECPPCJSONDTS.cJSON_Print( p );
        NODECPPCJSONDTS.cJSON_Minify(cStr);
        let str :string = "";

        //@ts-ignore
        NodeCppDefine.NODE_CPP_TOOLBOX_DEFINE_CopyString( str, cStr );
        //@ts-ignore
        NodeCppDefine.NODE_CPP_TOOLBOX_DEFINE_DeleteCharPtr( cStr );
        return str;
      }

      export function stringify2( p: NODECPPCJSONDTS.cJSON ) : string  {
        let cStr = NODECPPCJSONDTS.cJSON_Print( p );
        let str :string = "";

        //@ts-ignore
        NodeCppDefine.NODE_CPP_TOOLBOX_DEFINE_CopyString( str, cStr );
        //@ts-ignore
        NodeCppDefine.NODE_CPP_TOOLBOX_DEFINE_DeleteCharPtr( cStr );
        return str;
      }

      export function Delete( p: NODECPPCJSONDTS.cJSON )  {
        NODECPPCJSONDTS.cJSON_Delete( p );
      }
      
      export function getArraySize( p: NODECPPCJSONDTS.cJSON ) : bigint  {
        return BigInt( NODECPPCJSONDTS.cJSON_GetArraySize( p ) );
      }
      
      export function getArrayItem( pArr: NODECPPCJSONDTS.cJSON, index : number ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_GetArrayItem( pArr, index );
      }

      export function getObjectItem( pArr: NODECPPCJSONDTS.cJSON, key: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_GetObjectItem( pArr, key );
      }

      export function hasObjectItem( pArr: NODECPPCJSONDTS.cJSON, key: string ) : number {
        return NODECPPCJSONDTS.cJSON_HasObjectItem( pArr, key );
      }
      
      // export function errorStr( ) : string {
      //   return NODECPPCJSONDTS.cJSON_GetErrorPtr( );
      // }

      export function getString( p: NODECPPCJSONDTS.cJSON ) : string {
        return NODECPPCJSONDTS.cJSON_GetStringValue( p );
      }

      export function getNumber( p: NODECPPCJSONDTS.cJSON ) : number {
        return NODECPPCJSONDTS.cJSON_GetNumberValue( p );
      }

      
      export function isNULLPtr( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( !p ) {
          return true;
        }

        return false;
      }

      export function isBoolean( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsBool( p ) ) {
          return true;
        }

        return false;
      }
      
      export function isFalse( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsFalse( p ) ) {
          return true;
        }

        return false;
      }

      export function isTrue( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsTrue( p ) ) {
          return true;
        }

        return false;
      }

      export function Object() : NODECPPCJSONDTS.cJSON {
        return NODECPPCJSONDTS.cJSON_CreateObject();
      }

      export function ObjectAddString( obj: NODECPPCJSONDTS.cJSON, key: string, val: string ) {
        NODECPPCJSONDTS.cJSON_AddStringToObject(obj, key, val);
      }

      export function ObjectAddNumber( obj: NODECPPCJSONDTS.cJSON, key: string, val: number ) {
        NODECPPCJSONDTS.cJSON_AddNumberToObject(obj, key, val);
      }

      export function ObjectAddBoolean( obj: NODECPPCJSONDTS.cJSON, key: string, val: boolean ) {
        if( val ) {
          NODECPPCJSONDTS.cJSON_AddTrueToObject(obj, key);
        } else {
          NODECPPCJSONDTS.cJSON_AddFalseToObject(obj, key);
        }
      }

      export function ObjectGetString( obj: NODECPPCJSONDTS.cJSON, key: string ): string {
        return NODECPPCJSONDTS.cJSON_GetStringValue( 
          NODECPPCJSONDTS.cJSON_GetObjectItem(obj, key) );
      }

      export function ObjectGetNumber( obj: NODECPPCJSONDTS.cJSON, key: string ): number {
        return NODECPPCJSONDTS.cJSON_GetNumberValue( 
          NODECPPCJSONDTS.cJSON_GetObjectItem(obj, key) );
      }
      
      export function ObjectGetBoolean( obj: NODECPPCJSONDTS.cJSON, key: string ): boolean {
        let tf = NODECPPCJSONDTS.cJSON_IsTrue( 
          NODECPPCJSONDTS.cJSON_GetObjectItem(obj, key) );

          if( tf ) {
            return true;
          }

          return false;
      }
      
      export function isInvalid( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsInvalid( p ) ) {
          return true;
        }

        return false;
      }    

      export function valIsNull( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsNull( p ) ) {
          return true;
        }

        return false;
      }

      export function isNumber( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsNumber( p ) ) {
          return true;
        }

        return false;
      }

      export function isString( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsString( p ) ) {
          return true;
        }

        return false;
      }
      
      export function isArray( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsArray( p ) ) {
          return true;
        }

        return false;
      }     

      export function isObject( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsObject( p ) ) {
          return true;
        }

        return false;
      }

      export function isRaw( p : NODECPPCJSONDTS.cJSON ) : boolean {
        if( NODECPPCJSONDTS.cJSON_IsRaw( p ) ) {
          return true;
        }

        return false;
      }

      export function createNull( ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateNull( );
      }

      export function createTrue( ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateTrue( );
      }
      
      export function ceateFalse( ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateFalse( );
      }    

      export function createBool( bl : boolean ) : NODECPPCJSONDTS.cJSON  {
        if( bl === true ) {
          return  NODECPPCJSONDTS.cJSON_CreateBool( 0 );
        }

        return NODECPPCJSONDTS.cJSON_CreateBool( 1 );
      }

      export function createNumber( n : number ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateNumber( n );
      }
      
      export function createString( str : string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateString( str );
      }
            
      export function createRaw( raw : string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateRaw( raw );
      }

      export function createArray( ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateArray( );
      }
      
      export function createObject( ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_CreateObject( );
      }

      export function addItemToArray( pArr: NODECPPCJSONDTS.cJSON, item: NODECPPCJSONDTS.cJSON) : boolean  {
        if( NODECPPCJSONDTS.cJSON_AddItemToArray( pArr, item ) ) {
          return true;
        }

        return false;
      }
      
      export function addItemToObject( pObj: NODECPPCJSONDTS.cJSON, key: string, item: NODECPPCJSONDTS.cJSON) : boolean  {
        if( NODECPPCJSONDTS.cJSON_AddItemToObject( pObj, key, item ) ) {
          return true;
        }

        return false;
      }

      export function addNullToObject( obj: NODECPPCJSONDTS.cJSON, name: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddNullToObject( obj, name );
      }
      
      export function addTrueToObject( obj: NODECPPCJSONDTS.cJSON, name: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddTrueToObject( obj, name );
      }
            
      export function addFalseToObject( obj: NODECPPCJSONDTS.cJSON, name: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddFalseToObject( obj, name );
      }
                  
      export function addNumberToObject( obj: NODECPPCJSONDTS.cJSON, name: string, num :number ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddNumberToObject( obj, name, num );
      }

      export function addStringToObject( obj: NODECPPCJSONDTS.cJSON, name: string, str :string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddStringToObject( obj, name, str );
      }

      export function addRawToObject( obj: NODECPPCJSONDTS.cJSON, name: string, raw :string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddRawToObject( obj, name, raw );
      }

      export function addObjectToObject( obj: NODECPPCJSONDTS.cJSON, name: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddObjectToObject( obj, name );
      }

      export function addArrayToObject( obj: NODECPPCJSONDTS.cJSON, name: string ) : NODECPPCJSONDTS.cJSON  {
        return NODECPPCJSONDTS.cJSON_AddArrayToObject( obj, name );
      }
}

