#include "./node_cpp_load_dll.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #include <dlfcn.h>
#endif


namespace NodeCppLoadDll
{

void* node_cpp_lib_load_dll( const char* dllPath )
{
#ifdef _WIN32
    return LoadLibrary(dllPath);
#else
    return dlopen(dllPath, RTLD_NOW);
#endif
}


void* node_cpp_lib_get_dll_func_ptr( void* dllHandl, const char* functionName )
{
#ifdef _WIN32
    return GetProcAddress((HMODULE)dllHandl, functionName );
#else
    return dlsym(dllHandl, functionName);
#endif
}

bool node_cpp_lib_free_dll( void* dllHandl )
{
    int result = 0;
#ifdef _WIN32
    result = FreeLibrary((HMODULE)dllHandl);
    if( result >= 1 ) { return true;  }
    if( result <  1 ) { return false; }
#else
    result = dlclose(dllHandl);
    if( result >= 0 ) { return true;  }
    if( result <  0 ) { return false; }
#endif

    return false;
}

}