#ifndef NODE_CPP_LOAD_DLL_H
#define NODE_CPP_LOAD_DLL_H

namespace NodeCppLoadDll
{
    void* node_cpp_lib_load_dll( const char* dllPath );
    void* node_cpp_lib_get_dll_func_ptr( void* dllHandl, const char* functionName );
    bool  node_cpp_lib_free_dll( void* dllHandl );
}

#endif
