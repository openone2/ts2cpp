#ifndef NODE_CPP_TOOLBOX_DEFINE_H
#define NODE_CPP_TOOLBOX_DEFINE_H

#define NODE_CPP_TOOLBOX_DEFINE_DeleteCharPtr(charPtr) {  \
  if( charPtr ) {                                         \
    delete []charPtr;                                     \
    charPtr = nullptr;                                    \
  }                                                       \
}                                                         \

#define NODE_CPP_TOOLBOX_DEFINE_CopyString(stdString, charPtr) {     \
  if( charPtr ) {                                                    \
    stdString = charPtr;                                             \
  }                                                                  \
}                                                                    \

#endif
