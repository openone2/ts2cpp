cd tiny_app &&                                                          ^
clang++ -std=c++20                                                 ^
-Wno-deprecated                                                       ^
-Wno-microsoft-cast                                                   ^
test.cpp                                                                        ^
./../cpplib/third_party/cJSON/cJSON.c                       ^
./../cpplib/node_cpp_lib/node_cpp_json.cpp              ^
./../cpplib/node_cpp_lib/dll/node_cpp_load_dll.cpp   ^
-I ./../cpplib                                                                  ^
-I ./../cpplib/third_party/cJSON                                   ^
-I ./../cpplib/node_cpp_lib                                           ^
-I ./../cpplib/node_cpp_lib/dll                                      ^
-I ./                                                                               ^
-I ./../                                                        ^
-o test.exe  &&  test.exe && cd ..