import * as ts from 'typescript';

export class Helpers {
    public static isConstOrLet(node: ts.Node): boolean {
        return (node.flags & ts.NodeFlags.Let) === ts.NodeFlags.Let || (node.flags & ts.NodeFlags.Const) === ts.NodeFlags.Const;
    }

    public static isConst(node: ts.Node): boolean {
        return (node.flags & ts.NodeFlags.Const) === ts.NodeFlags.Const;
    }

    public static isLet(node: ts.Node): boolean {
        return (node.flags & ts.NodeFlags.Let) === ts.NodeFlags.Let;
    }

    public static correctFileNameForCxx(filePath: string): string {
        // fix filename
        let fileNameIndex = filePath.lastIndexOf('\\');
        if (fileNameIndex === -1) {
            fileNameIndex = filePath.lastIndexOf('/');
        }

        const fileName = fileNameIndex >= 0 ? filePath.substr(fileNameIndex + 1) : filePath;

        const extIndex = fileName.lastIndexOf('.');
        const fileNameNoExt = extIndex >= 0 ? fileName.substr(0, extIndex) : fileName;

        const fileNameFixed = fileNameNoExt;

        // rebuild filePath
        const beginPath = fileNameIndex >= 0 ? filePath.substr(0, fileNameIndex + 1) : '';
        const endExt = extIndex >= 0 ? fileName.substr(extIndex) : '';

        return beginPath + fileNameFixed + endExt;
    }

    public static cleanUpPath(path: string) {
        if (!path) {
            return;
        }

        if (path.charAt(1) === ':' && path.charAt(0).match('[A-Z]')) {
            path = path.charAt(0).toLowerCase() + path.substr(1);
        }

        return path.replace(/\\/g, '/');
    }

    private static findSameStartsWith( str1: string, str2: string ) {
        let longArr : string[] = [];
        let shortArr : string[] = [];
        let sameStartsWith : string[] = [];
        
        if( str1.split("/").length >= str2.split("/").length ) {
            longArr = str1.split("/");
            shortArr = str2.split("/");
        } else {
            longArr = str2.split("/");
            shortArr = str1.split("/");
        }

        for (let i = 0; i < shortArr.length; i++) {            
            if( shortArr[i] != longArr[ i ] ) {
                break;
            }

            sameStartsWith.push( shortArr[ i ] );
        }

        let sameStartsWithStr= "";
        for (let index = 0; index < sameStartsWith.length; index++) {
            const element = sameStartsWith[index];

            if( index === 0 ) {
                sameStartsWithStr = element;
            }

            if( index != 0 ) {
                sameStartsWithStr = sameStartsWithStr + "/" + element;
            }
        }

        return sameStartsWithStr;
    }

    public static getSubPath(filePath: string, rootPath: string) {
        if (!rootPath) {
            return filePath;
        }

        if (rootPath[rootPath.length - 1] === '/' || rootPath[rootPath.length - 1] === '\\') {
            rootPath = rootPath.substr(0, rootPath.length - 1);
        }

        //const positionFrom = rootPath.length + (rootPath.length > 0 && (rootPath[0] === '/' || rootPath[0] === '\\') ? 0 : 1);
        const positionFrom = Helpers.findSameStartsWith(filePath, rootPath ).length;

        let fileSubPath = rootPath.length > 0 && filePath.startsWith(Helpers.findSameStartsWith( filePath, rootPath ))
            ? filePath.substring(positionFrom)
            : filePath;

            if( fileSubPath.length > 0 && fileSubPath[ 0 ] === "/" ) {
                fileSubPath = fileSubPath.substring( 1 );
            }

        return fileSubPath;
    }
}
