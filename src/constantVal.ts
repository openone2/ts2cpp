

const NodeCppBindThirdParty = [{
    scope : [
        "NODECPPCJSONDTS"
    ],

    pointer : [
        "cJSON"
    ]
}];


export default {
    NodeCppBindThirdParty: NodeCppBindThirdParty
}