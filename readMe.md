### Note:
#### base on TypeScript2Cxx
`https://github.com/ASDAlexander77/TypeScript2Cxx`

===========================

### difference
ts2cpp supports llvm(win-msvc) calng++13.0.0<br/>
supports bigint<br/>
supports cjson<br/>

===========================

License
-------

TypeScript2Cxx is licensed under the MIT license.<br/>
ts2cpp is licensed under the MIT license, too.

Quick Start
-----------

1) Build Project

```
npm install
npm run build
```

2) Compile test.ts

```
tiny_app_compile.cmd
```

Now you have test.cpp and test.h


3) Compile it.

llvm(win-msvc) calng++13.0.0
```
tiny_app_run.cmd
```

4) Run it.

```
test.exe
```

Result:
```
Hello, my name is Howard and I work in Sales.
```

Enjoy it. 
