function test02() : void {
    
    try {
        console.log( parseInt( "999999990999999" ) );
    } catch ( error ) {
        console.log( error );
        console.log( typeof error );
    }

    console.log( parseFloat( "10.2" ) );
    console.log( parseFloat( "10.2a" ) );
    console.log( parseFloat( "a10.2" ) );
    console.log( parseInt( "10.2" ) );
    console.log( parseInt( "10.2a" ) );
    console.log( parseInt( "10" ) );
    console.log( isNaN( parseInt( "a10" ) ) );
}

test02();