import { NodeCppJSON } from "./../cpplib/node_cpp_lib/node_cpp_json";

function e09() {
  const jsonStr = "{\"key1\":1, \"key2\":[1,2,3], \"key3\": { \"key3_1\": \"hello\" }}";  

  let jsonObj = NodeCppJSON.parse( jsonStr );
  if( !jsonObj ) {
    console.log( "json parse error.");
    return;
  }

  console.log( "key1:", NodeCppJSON.ObjectGetNumber( jsonObj, "key1" ) );
  console.log( "key2.length:",
    NodeCppJSON.getArraySize( NodeCppJSON.getObjectItem( jsonObj, "key2" ) ),
    "   key2[0]:",
    NodeCppJSON.getNumber( NodeCppJSON.getArrayItem( NodeCppJSON.getObjectItem( jsonObj, "key2" ), 0 ) )
   );
  console.log( "key3.key3_1:", 
  NodeCppJSON.ObjectGetString( NodeCppJSON.getObjectItem( jsonObj, "key3" ), "key3_1") );

  console.log( "jsonStr: ", NodeCppJSON.stringify( jsonObj ) );
  NodeCppJSON.Delete( jsonObj );

  //@ts-ignore
  jsonObj = nullptr; //c++ code
}

e09();


function e09b() {
  console.log("========== e09b ===========");

  let jsonObj = NodeCppJSON.Object();
  if( !jsonObj ) {
    console.log("create json object error");
    return;
  }

  NodeCppJSON.ObjectAddBoolean( jsonObj, "keyTrue" , true );
  NodeCppJSON.ObjectAddNumber( jsonObj, "keyNum" , 123 );
  NodeCppJSON.ObjectAddString( jsonObj, "keyStr" , "str_string" );

  const arrary1 = NodeCppJSON.addArrayToObject( jsonObj, "key_arr" );
  if( !arrary1 ) {
    console.log("create json array error !" );
  } else {
    NodeCppJSON.addItemToArray( arrary1, NodeCppJSON.createNumber(111) );
    NodeCppJSON.addItemToArray( arrary1, NodeCppJSON.createNumber(222) );
    NodeCppJSON.addItemToArray( arrary1, NodeCppJSON.createNumber(333) );
  }

  
  const obj1 = NodeCppJSON.addObjectToObject( jsonObj, "key_obj" );
  if( !obj1 ) {
    console.log("create json obj1 error !" );
  } else {
    NodeCppJSON.addItemToObject( obj1, "obj_key1",  NodeCppJSON.createNumber(111) );
    NodeCppJSON.addItemToObject( obj1, "obj_key2",  NodeCppJSON.createNumber(222) );
    NodeCppJSON.addItemToObject( obj1, "obj_key3",  NodeCppJSON.createNumber(333) );
  }

  console.log( "stringify:", NodeCppJSON.stringify( jsonObj ) );
  console.log(" ---- ");
  console.log( "jsonStr2: ", NodeCppJSON.stringify2( jsonObj ) );

  NodeCppJSON.Delete( jsonObj );

  //@ts-ignore
  jsonObj = nullptr; //c++ code
}


e09b();
console.log("");
console.log("END!")