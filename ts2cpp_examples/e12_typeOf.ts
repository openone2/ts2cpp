function e12() {
  let arr : string[] = [];
  console.log( "arr type         :" + Object.prototype.toString.call( arr ) );
  console.log( "arr typeOf       :" + typeof( arr ) );
  console.log( "" );

  let num = Number(1.2);
  console.log( "num type         :" + Object.prototype.toString.call( num ) );
  console.log( "num typeOf       :" + typeof( num ) );
  console.log( "" );

  let bInt = BigInt(2);
  console.log( "BigIntNum type   :" + Object.prototype.toString.call( bInt ) );
  console.log( "BigIntNum typeOf :" + typeof( bInt ) );
  console.log( "" );

  let bol : boolean = true;
  console.log( "bol type         :" + Object.prototype.toString.call( bol ) );
  console.log( "bol typeOf       :" + typeof( bol ) );
  console.log( "" );

  let str = "str";
  console.log( "str type         :" + Object.prototype.toString.call( str ) );
  console.log( "str typeOf       :" + typeof( str ) );
  console.log( "" );

  let obj = {"k":"k"};
  console.log( "obj type         :" + Object.prototype.toString.call( obj ) );
  console.log( "obj typeOf       :" + typeof( obj ) );
  console.log( "" );

  let undef = undefined;
  console.log( "undef type       :" + Object.prototype.toString.call( undef ) );
  console.log( "undef typeOf     :" + typeof( undef ) );
  console.log( "" );

  console.log( "=== any type ===" );
  let any1 = {
    "n" : 1.0,
    "b" : BigInt(2),
    "t" : <boolean>false,
    "s" : "str",
    "a" : <any> {
      "y" : "y"
    },
    "r" : <any>[100],
    "u" : undefined,
    "o" : <object>{"k":"k"}
  };

  for (const key in any1 ) {
    console.log( `key[${key}] type   :${Object.prototype.toString.call( any1[ key ] )}`);
    console.log( `key[${key}] typeof :${typeof any1[ key ]}`);
    console.log("");
  }

  console.log("END!");
}

e12();

