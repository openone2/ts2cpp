function e08() {
  let timeNow = new Date();

  console.log( `${timeNow.getFullYear()}-${timeNow.getMonth()+1}-${timeNow.getDate()} ${timeNow.getHours()}:${timeNow.getMinutes()}:${timeNow.getSeconds()}:${timeNow.getMilliseconds()}`);
  console.log( BigInt(timeNow.getTime()) );

  console.log("");
  let time2 = new Date("2021-02-28 10:00:458");
  console.log( `${time2.getFullYear()}-${time2.getMonth()+1}-${time2.getDate()} ${time2.getHours()}:${time2.getMinutes()}:${time2.getSeconds()}:${time2.getMilliseconds()}`);
  console.log( BigInt(time2.getTime()) );

  try {
      console.log("");
      let time3 = new Date("2021-02-28 10:00:12");
      console.log( `${time3.getFullYear()}-${time3.getMonth()+1}-${time3.getDate()} ${time3.getHours()}:${time3.getMinutes()}:${time3.getSeconds()}:${time3.getMilliseconds()}`);
      console.log( BigInt(time3.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }

  try {
    console.log("");
    let time4 = new Date("2021-02-28 10:00:aa");
    console.log( `${time4.getFullYear()}-${time4.getMonth()+1}-${time4.getDate()} ${time4.getHours()}:${time4.getMinutes()}:${time4.getSeconds()}:${time4.getMilliseconds()}`);
    console.log( BigInt(time4.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }

  try {
    console.log("");
    let time5 = new Date(1614477645458);
    console.log( `${time5.getFullYear()}-${time5.getMonth()+1}-${time5.getDate()} ${time5.getHours()}:${time5.getMinutes()}:${time5.getSeconds()}:${time5.getMilliseconds()}`);
    console.log( BigInt(time5.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }


  try {
    console.log("");
    let time6 = new Date(Number(1614477645459));
    console.log( `${time6.getFullYear()}-${time6.getMonth()+1}-${time6.getDate()} ${time6.getHours()}:${time6.getMinutes()}:${time6.getSeconds()}:${time6.getMilliseconds()}`);
    console.log( BigInt(time6.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }


  try {
    console.log("");
    let time7 = new Date(Number(1614477645459));
    console.log( `time7: ${time7.getFullYear()}-${time7.getMonth()+1}-${time7.getDate()} ${time7.getHours()}:${time7.getMinutes()}:${time7.getSeconds()}:${time7.getMilliseconds()}`);
    console.log( BigInt(time7.getTime()) );

    console.log( `time7 set time ${BigInt(time7.setTime(1614477645460))}` );
    console.log( `time7: ${time7.getFullYear()}-${time7.getMonth()+1}-${time7.getDate()} ${time7.getHours()}:${time7.getMinutes()}:${time7.getSeconds()}:${time7.getMilliseconds()}`);
    console.log( BigInt(time7.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }


  try {
    console.log("");
    let time8 = new Date(1614477645461);
    console.log( `time8: ${time8.getFullYear()}-${time8.getMonth()+1}-${time8.getDate()} ${time8.getHours()}:${time8.getMinutes()}:${time8.getSeconds()}:${time8.getMilliseconds()}`);
    console.log( BigInt(time8.getTime()) );

    console.log( `time8 set time ${BigInt(time8.setTime(1614477645462))}` );
    console.log( `time8: ${time8.getFullYear()}-${time8.getMonth()+1}-${time8.getDate()} ${time8.getHours()}:${time8.getMinutes()}:${time8.getSeconds()}:${time8.getMilliseconds()}`);
    console.log( BigInt(time8.getTime()) );
  }catch (error) {
      console.log( "err:" + error );
  }


  try {
    console.log("");
    let time9 = new Date(1614477645462);
    console.log( `time9: ${time9.getFullYear()}-${time9.getMonth()+1}-${time9.getDate()} ${time9.getHours()}:${time9.getMinutes()}:${time9.getSeconds()}:${time9.getMilliseconds()}`);
    console.log( `time9.getTime() === timeNow.getTime() [${BigInt(time9.getTime())}===${BigInt(timeNow.getTime())}] : ${time9.getTime() === timeNow.getTime()}` );
    console.log( `time9.getTime() >= timeNow.getTime() [${BigInt(time9.getTime())}>=${BigInt(timeNow.getTime())}]   : ${time9.getTime() >= timeNow.getTime()}` );
    console.log( `time9.getTime() <= timeNow.getTime() [${BigInt(time9.getTime())}<=${BigInt(timeNow.getTime())}]   : ${time9.getTime() <= timeNow.getTime()}` );
    console.log( `time9.getTime() > timeNow.getTime() [${BigInt(time9.getTime())}>${BigInt(timeNow.getTime())}]     : ${time9.getTime() > timeNow.getTime()}` );
    console.log( `time9.getTime() < timeNow.getTime() [${BigInt(time9.getTime())}<${BigInt(timeNow.getTime())}]     : ${time9.getTime() < timeNow.getTime()}` );
    console.log( `time9.getTime() !== timeNow.getTime() [${BigInt(time9.getTime())}!==${BigInt(timeNow.getTime())}] : ${time9.getTime() !== timeNow.getTime()}` );

  }catch (error) {
      console.log( "err:" + error );
  }

  try {
    console.log("");
    let nowNumber = Date.now();
    const timeNow = new Date(nowNumber);
    console.log( `timeNow: ${timeNow.getFullYear()}-${timeNow.getMonth()+1}-${timeNow.getDate()} ${timeNow.getHours()}:${timeNow.getMinutes()}:${timeNow.getSeconds()}:${timeNow.getMilliseconds()}`);
 
  }catch (error) {
      console.log( "err:" + error );
  }

}

e08();