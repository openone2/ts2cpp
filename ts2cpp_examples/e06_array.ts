class MyClass {
  n_ : number = 0;
  constructor( n : number ) {
    this.n_ = n;
  }

  public Log() {
    console.log( `this.n_[${this.n_}]` );
  }
}

function myClassTest( arr : MyClass[] ) {
  for (const iterator of arr) {
      iterator.Log();
  }
}


function foo1() {
  console.log(" ");
  console.log("======= foo1 =======");

  let arr1 : any[] = [BigInt(0)];
  arr1.push( BigInt(111) );
  arr1.push( BigInt(222) );
  arr1.push( BigInt(333) );
  arr1.push( BigInt(444) );

  let q = {
    "m" : {
      "mt": {
        "arr" : arr1
      }
    }
  };

  let qArr = q.m.mt.arr
  for (let index = 0; index < qArr.length; index++) {
    let element = qArr[index];
    console.log( element );
  }
  console.log("=============");
  console.log(" ");

  for (let v of arr1) {
    console.log(v);
    ++v;
  }
  console.log("=============");
  console.log(" ");

  for (let index = 0; index < qArr.length; index++) {
    let element = <bigint>qArr[index];
    console.log( element );
  }
  console.log("=============");
  console.log(" ");

  for (let index = 0; index < qArr.length; index++) {
    let element = <bigint>qArr[index];
    console.log( <any>element === <any>BigInt(1) );
  }

}


interface testInter {
  m  : MyClass[],
  arr: bigint[]
}

function foo2() {
  console.log(" ");
  console.log("======= foo2 =======");
  type MArr = MyClass;

  const myClassArr           = [ new MyClass( 0.01 ), new MyClass( 0.09 ) ];
  const myClassArr2 : MArr[] = [ new MyClass( 0.01 ), new MyClass( 0.09 ) ];
  const bigintArr  = [ BigInt(0) ];

  let test : testInter = {
    m   : myClassArr,
    arr : bigintArr,
  };
  test.m.push( new MyClass( 3.00 ) );
  myClassTest( test.m );
}

function e06() {
  foo1();
  foo2();
  console.log("end!");
}

e06();