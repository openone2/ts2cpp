function cb( func_ref: ()=>void , func: ()=>void ) { /*ts_to_cpp_param_ref_var*/
  func_ref();
}

class Cls {
  public _v : bigint = BigInt(90);
  constructor() {
  }
}


interface o1Inter {
  k : Cls,
  j : string
}

function e13() {
  let str = "str1";
  let n1 = Number( 1 );
  let b1 = BigInt( 30 );
  let o1 : o1Inter = {
    k : new Cls(),
    j : "jStr"
  };

  console.log("str     :" + str);
  console.log("n1      :" + n1);
  console.log("b1      :" + b1);
  console.log("o1.k._v :" + o1.k._v);
  console.log("o1.j    :" + o1.j);
  
  cb(() => {
    str = str + str;
    n1++;
    b1++;
    ++o1.k._v;
    o1.j = o1.j + o1.j;
  }, () => {});

  console.log("");
  console.log("str     :" + str);
  console.log("n1      :" + n1);
  console.log("b1      :" + b1);
  console.log("o1.k._v :" + o1.k._v);
  console.log("o1.j    :" + o1.j);
  console.log("END!");
}

e13();

