function e07() {
  let a1 = BigInt(111);
  const a2 = BigInt(222);
  const a4 = <any>BigInt(111);
  let a5 = <any>BigInt(111);

  console.log( "a1 === a2 :" + (a1 === a2) );
  console.log(" ");

  console.log( "a1 === <bigint>a4 :" + (a1 === <bigint>a4) );
  console.log( "a1 / a4 :" + (a1 / a4) );
  console.log( "a1 % a4 :" + (a1 % a4) );
  console.log( "a5 === a4 :" + (a5 === a4) );
  console.log(" ");

  console.log( "a1 >= a2 :" + (a1 >= a2));
  console.log( "a1 >= <bigint>a4 :" + (a1 >= <bigint>a4) );
  console.log( "a5 >= a4 :" + (a5 >= a4) );
  console.log(" ");

  console.log( "a1 < a2 :" + (a1 < a2) );
  console.log( "a1 < <bigint>a4 :" + (a1 < <bigint>a4) );
  console.log( "a5 <a4 :" + (a5 <a4) );

  console.log(" ");
  console.log( "(<bigint>a5) += BigInt(1) :" + ((<bigint>a5) += BigInt(1)) );
  console.log( "(<bigint>a5) *= BigInt(2) :" + ((<bigint>a5) *= BigInt(2)) );
  console.log( "(<bigint>a5) /= BigInt(2) :" + ((<bigint>a5) /= BigInt(2)) );
  console.log( "(<bigint>a5) -= BigInt(2) :" + ((<bigint>a5) -= BigInt(2)) );

  console.log(" ");
  let a5_ = <bigint>a5;
  console.log( "a5_ += BigInt(1) :" + (a5_ += BigInt(1)) );
  console.log( "a5_ *= BigInt(2) :" + (a5_ *= BigInt(2)) );
  console.log( "a5_ /= BigInt(2) :" + (a5_ /= BigInt(2)) );
  console.log( "a5_ -= BigInt(2) :" + (a5_ -= BigInt(2)) );
  console.log( "a5:" + a5 );

  console.log(" ");
  console.log( "a1 += BigInt(1) :" + (a1 += BigInt(1)) );
  console.log( "a1 *= BigInt(2) :" + (a1 *= BigInt(2)) );
  console.log( "a1 /= BigInt(2) :" + (a1 /= BigInt(2)) );
  console.log( "a1 -= BigInt(2) :" + (a1 -= BigInt(2)) );

  console.log( "END!" );
}

e07();