function test03(): void {

    try {
        console.log(parseInt("999999990999999"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("999999990999999"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("10.2"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("11.2a"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("a12.2"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("13.2"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }


    try {
        console.log(BigInt("14.2a"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("15"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt("a16"));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    try {
        console.log(BigInt(Number(345)));
    } catch (error) {
        console.log(error);
        console.log(typeof error);
    }

    const obj = {
        "b" : BigInt( 100 ),
        "c" : "456",
        "d" : true,
        "e" : NaN,
    };
    try {
        console.log( `obj.b typeof: ${typeof obj.b}` );
        console.log( BigInt(obj.c) );
        console.log( BigInt(obj.d) );

    } catch (error) {
        console.log(error);
    }

    try {
        console.log( BigInt(obj.e) );
    } catch (error) {
        console.log(error);
    }

    try {
        console.log( "toString():" + BigInt(obj.c).toString()  );
    } catch (error) {
        console.log(error);
    }

    try {
        console.log( "to string:" + BigInt(obj.c)  );
    } catch (error) {
        console.log(error);
    }

    console.log("END");
}

test03();