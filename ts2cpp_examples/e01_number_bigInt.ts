function test01() : void {
    let n = 2147483647;
    let m = 2147483648;
    let v = BigInt(2147483648);

    let c = Number( "1023" );
    let x = Number( "2147483648" );
    let z = Number( "ac214" );
    let a = Number( BigInt( 1024 ) );

    console.log( n );
    console.log( m );
    console.log( v );

    console.log( c );
    console.log( x );
    console.log( z );
    console.log( a );
}

test01();