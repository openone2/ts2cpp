function refTest( str1 : string, str2_ref: string,                      /*ts_to_cpp_param_ref_var*/
    any1 : any, any_ref : any, obj : object, obj_ref : object,        /*ts_to_cpp_param_ref_var*/
    num_ref: number, func_ref : ( n: number, n_ref:number ) => void ) { /*ts_to_cpp_param_ref_var*/

    str1           = str1 + "_refTest";
    str2_ref       = str2_ref + "_refTest";
    any1[ "n" ]    = 14;
    any_ref[ "n" ] = 13;
    obj[ "n" ]++;
    obj_ref[ "n" ]++;

    func_ref( 1, num_ref );
}


function e11() {
 
  let str1     = "str1";
  let str2     = "str2";
  let any1     = <any> {
    "n" : 1
  };
  let any_ref  = <any> {
    "n" : 0
  };
  let obj     = {
    "n" : 33
  };
  let obj_ref = {
    "n" : 44
  };
  let num_ref = Number(99);

  console.log( "=== before ===" );
  console.log( `str1      : [${str1}]` );
  console.log( `str2      : [${str2}]` );
  console.log( `any1.n    : [${any1.n}]` );
  console.log( `any_ref.n : [${any_ref.n}]` );
  console.log( `obj.n     : [${obj.n}]` );
  console.log( `obj_ref.n : [${obj_ref.n}]` );
  console.log( `n_ref     : [${num_ref}]` );

  refTest( str1, str2, any1, any_ref, obj, obj_ref,   /*ts_to_cpp_param_ref_var*/
    num_ref, ( n: number, n_ref: number ) : void => { /*ts_to_cpp_param_ref_var*/
      n_ref = 199;
  });

  console.log( "" );
  console.log( "=== after ===" );
  console.log( `str1      : [${str1}]` );
  console.log( `str2      : [${str2}]` );
  console.log( `any1.n    : [${any1.n}]` );
  console.log( `any_ref.n : [${any_ref.n}]` );
  console.log( `obj.n     : [${obj.n}]` );
  console.log( `obj_ref.n : [${obj_ref.n}]` );
  console.log( `n_ref     : [${num_ref}]` );

  console.log("END!");
}

e11();

