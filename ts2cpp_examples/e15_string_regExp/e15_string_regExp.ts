import "./c_string_regExp";

function e15() {
  let str1 = "str1_replace_and_replace_end";
  console.log(`before[${str1}]  after:` + str1.replace(RegExp("replace"), "") );

  console.log("");

  {
    let str2 = "str2Part1";
    let str2AfterReg = "";
    let str2Arr = str2.match("(str2)(.*)");
    if( str2Arr ) {
       str2Arr.forEach( e => {
        str2AfterReg = str2AfterReg +  `[${e}] `;
       })
    }
    console.log(`before[${str2}]  after:` + str2AfterReg );
    console.log("");
    
    str2AfterReg = "";
    //@ts-ignore
    const len = NodeCppDefine.GETARRYLEN(str2Arr);
    for( let i = 0; i < len; i++ ) {
      //@ts-ignore
      str2AfterReg = str2AfterReg + `[${NodeCppDefine.GETARRYELE(str2Arr, i)}] `;
    }

    console.log(`before[${str2}]  after:` + str2AfterReg );
  }


}

e15();
console.log("END!!");