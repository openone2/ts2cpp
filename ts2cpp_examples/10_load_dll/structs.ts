export module structs {
    export interface StructArray {
        big : bigint;
    }

    export interface Struct1 {
        b : bigint;
        n : number;
        str : string;
        aCount : bigint;
        a : StructArray[];
    }
}