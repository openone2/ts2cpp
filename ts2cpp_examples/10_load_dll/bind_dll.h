#ifndef BIND_DLL_H
#define BIND_DLL_H
#include "../../node_cpp_lib/dll/node_cpp_load_dll.h"
#include <core.h>
#include <iostream>

using namespace js;

namespace bindDLL {
    static inline void* pDll = nullptr;

    static inline bool initDLL( const char* dllPath )
    {
        auto p = NodeCppLoadDll::node_cpp_lib_load_dll( dllPath );
        if( !p ) {
            return false;
        }

        pDll = p;
        return true;
    }

    static inline bool freeDLL()
    {
        if( !pDll ) {
            return -1;
        }
        return NodeCppLoadDll::node_cpp_lib_free_dll( pDll );
    }

    static inline void hello( double n, long long bigN, const char* str, void(*cb)( double x, char* ptr ), js::array<js::number>& arr, int arrLen )
    {
        double* tmpArr = nullptr;
        if( arrLen > 0 )
        {
            tmpArr = (double*) &(*arr._values.get())[0];
            arr[2] = 2222.22;
        }
        
        auto pFunc = NodeCppLoadDll::node_cpp_lib_get_dll_func_ptr( bindDLL::pDll, "hello" );
        using f = void(*)(double n, long long bigN, const char* str, void(*cb)( double x, char* ptr ), double arr[], int arrLen);
        ((f)pFunc)(n, bigN, str, cb, tmpArr, arrLen);
    }

    static inline void printStruct(std::shared_ptr<structs::Struct1> ptr1)
    {
        const void* structOne = (const void*) &(ptr1.get()->b);
        auto pFunc = NodeCppLoadDll::node_cpp_lib_get_dll_func_ptr( bindDLL::pDll, "printStruct" );
        using f = void(*)(const void* structOne);
        ((f)pFunc)(structOne);
    }

    namespace DLLTest {
        struct testStruct
        {
            long long testLL = 0;
        };
    }
}


#define Get_TESTLL_VAL(ptr, testLLVal) {                             \
  if( ptr ) {                                                        \
    auto tmp = (bindDLL::DLLTest::testStruct*)ptr;                   \
    testLLVal = tmp->testLL;                                         \
  }                                                                  \
}                                                                    \

#define DEL_TEST_ARR(ptr) {                                          \
  if( ptr ) {                                                        \
    auto tmp = (bindDLL::DLLTest::testStruct*)ptr;                   \
    delete tmp;                                                      \
    tmp = nullptr;                                                   \
  }                                                                  \
}                                                                    \


#endif
