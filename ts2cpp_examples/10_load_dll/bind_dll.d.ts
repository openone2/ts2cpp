import { structs } from "./structs"
export module bindDLL {
    function initDLL( dllPath: string ) : boolean
    function hello( n:number, bigN: bigint, str: string, cb: (n: number, obj_ptr: string )=>void, arr: number[], arrLen : number ) : void
    function printStruct( structOne : structs.Struct1 ) : void
    function freeDLL() : boolean
}