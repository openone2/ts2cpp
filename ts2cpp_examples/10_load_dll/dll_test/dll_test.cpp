#include <stdio.h>
#include <iostream>
#include <vector>

namespace DLLTest {
    struct testStruct
    {
        long long testLL = 0;
    };

    struct Struct1
    {
        long long b = 0;
        double n = 0;
        struct 
        {
            char empty[8];
            std::string str;
        } string1;
        long long aCount = 0;

        struct
        {
            char empty[4];
            struct tmpArrStruct
            {
                char empty[16];
                long long testLL = 0;
            };
            std::shared_ptr<std::vector<std::shared_ptr<tmpArrStruct>>> a;
        } arr;
    };
}

extern "C" 
{ 

   __declspec(dllexport) void hello(double n, long long bigN, const char* str, void(*cb)( double x, char* ptr ), double arr[], int arrLen )
   { 
       printf("double:%f\n", n);
       printf("long long:%lld\n", bigN);
       printf("str:%s\n", str);
       for (size_t i = 0; i < (size_t)arrLen; i++)
       {
           printf("arr[%zu]:%f\n", i, arr[i]);
       }

       auto dt = new DLLTest::testStruct;
       dt->testLL = 98765432;
       char* pDt = (char*)dt;
       
       (*cb)( 909, pDt );
   }

   __declspec(dllexport) void printStruct(const void* structOne)
   { 
       if(structOne)
       {
            printf("\nprintStruct run!\n");
            DLLTest::Struct1* ptr = (DLLTest::Struct1*)structOne;
            printf("struct.b:%lld\n", ptr->b);
            printf("struct.n:%f\n", ptr->n);
            printf("struct.str:%s\n", ptr->string1.str.c_str());
            printf("struct.aCount:%lld\n", ptr->aCount);
            for (size_t i = 0; i < (size_t)ptr->aCount; i++)
            {
                auto vec = ptr->arr.a.get();
                printf("struct.a[%zu]:%lld\n", i, (*vec)[i]->testLL );
            }
       } 
   }
}