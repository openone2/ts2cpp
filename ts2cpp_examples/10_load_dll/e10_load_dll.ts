import { structs } from "./structs"
import { bindDLL } from "./bind_dll"

namespace f10NS {
    export function cb( n: number, obj_ptr: string ) {/*ts_to_cpp_param_base_var*/ 
        let val = BigInt(-1);
        //@ts-ignore
        NodeCppDefine.Get_TESTLL_VAL(obj_ptr, val);
        console.log( "callback run! n:" + n + " val:" + val );

        //@ts-ignore
        NodeCppDefine.DEL_TEST_ARR(obj_ptr);
        console.log( "callback end!" );
    }

    function cb2( n: number, obj_ptr: string, str: string, str_ptr: string, bn: bigint ) {
        /*cpp code:
            void cb2(js::number n, js::string obj_ptr, js::string str, js::string str_ptr, js::bigint bn);
        */
    }

    function cb3( n: number, obj_ptr: string, str: string, str_ptr: string, bn: bigint ) {/*ts_to_cpp_param_base_var*/
        /*cpp code:
            void cb3(double n, js::nodeCpp::objectPtrVal* obj_ptr, char* str, js::nodeCpp::objectPtrVal* str_ptr, long long bn);
        */
    }
}

function f10_load_dll() {
    if( !bindDLL.initDLL( "./dll_test.dll" ) ) {
        console.log("load dll error!");
        return
    }

    let arr = [ 66, 77, 88 ];
    bindDLL.hello(1.1, BigInt(2), "I am Tom.", f10NS.cb, arr, arr.length );

    {
        let e : structs.StructArray = {
            big : BigInt(333),
        }
        let structArray : structs.StructArray[] = [e];
        for( let i = 0; i < 5; i++ ) {
            let e : structs.StructArray = {
                big : BigInt(i),
            }
            structArray.push( e );
        }

        let structTmp : structs.Struct1 = {
            b : BigInt(111),
            n : Number(22.2),
            str : "struct_test",
            a : structArray,
            aCount : BigInt(structArray.length),
        };
        bindDLL.printStruct(structTmp);
    }

    bindDLL.freeDLL();
}

f10_load_dll();

console.log("END!");