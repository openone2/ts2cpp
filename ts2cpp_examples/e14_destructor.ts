class Base {
  constructor() { }

  destructor() {
    console.log("class Base. destructor()!" );
  }
}

class ClassExtend extends Base {
  constructor() {
    super();  
  }

  destructor() {
    console.log("class ClassExtend. destructor()!" );
  }
}


function e14() {
  let o = new ClassExtend();
}

e14();
console.log("END!!");