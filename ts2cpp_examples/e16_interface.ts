module M {
    export module M2 {
        export interface Inter1 {
            num : number;
            big : bigint;
            mclass: MClass | null;
        }
    
        export class MClass {
            str : string = "MClassStr";

            public static def() : MClass {
                return new MClass;
            }
        }
    
        export interface Inter2 {
            num : number;
            big : bigint;
            mclass: MClass;
            func() : void;
        }
    }
}


class CClass implements M.M2.Inter2 {
    num: number = 1;
    big: bigint = BigInt(2);
    mclass: M.M2.MClass = new M.M2.MClass;
    func(): void {
        console.log("CClass object func function. big:" + this.big);
    }
}

function Inter2Func( f :  M.M2.Inter2 ) {
    f.func();
}


function getBig(b : bigint, s : M.M2.Inter1 | null) : bigint {
    if(s){
        console.log("getBig s.big:" + s.big);
    }
    return BigInt(b);
}

function e16() : void {
    type shortI = M.M2.Inter1;

    let im2I : shortI = {
        num : 99,
        big : getBig(BigInt(888), null),
        mclass: null,
    }
    console.log( "im2I.big:" + im2I.big);
    if(!im2I.mclass) {
        im2I.mclass = M.M2.MClass.def();
        console.log( "im2I.mclass.str:" + im2I.mclass.str)
    }

    console.log("");
    let c = new CClass();
    c.func();
    Inter2Func(c);

    console.log("END!!!");
}

e16();